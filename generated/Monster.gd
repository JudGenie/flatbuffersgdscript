class_name Monster extends Table


enum Equipment {NONE = 0, Weapon = 1}
enum MonsterColor {RED = 0, GREEN = 1, BLUE = 2}


static func get_root_as_monster(buffer: StreamPeerBuffer, 
								obj: Monster,
								offset: int = -1) -> Monster:
	buffer.set_big_endian(false)
	
	if offset == -1:
		offset = buffer.get_position()
	
	buffer.seek(offset)
	var pos: int = buffer.get_u32()
	
	obj._assign(buffer, pos + offset)
	return obj


func _assign(buffer: StreamPeerBuffer, offset: int):
	_reset(buffer, offset)


func pos(obj: Vec3 = Vec3.new()) -> Vec3:
	var o: int = NumberTypes.UOffsetTFlags.cast(offset(4))
	if o != 0:
		obj._assign(self._buffer, self._pos + o)
		return obj
	else:
		return null


func mana() -> int:
	var o: int = NumberTypes.UOffsetTFlags.cast(offset(6))
	if o != 0:
		return getVal(NumberTypes.Int16Flags, self._pos + o)
	return 150


func hp() -> int:
	var o: int = NumberTypes.UOffsetTFlags.cast(offset(8))
	if o != 0:
		return getVal(NumberTypes.Int16Flags, self._pos + o)
	return 100


func name() -> String:
	var o: int = NumberTypes.UOffsetTFlags.cast(offset(10))
	if o != 0:
		return string(self._pos + o)
	return ""


func inventory(j: int) -> int:
	var o: int = NumberTypes.UOffsetTFlags.cast(offset(14))
	if o != 0:
		return getVal(NumberTypes.Uint8Flags, vector(o) + (j * 1)) & 0xFF
	return 0


func inventory_length() -> int:
	var o: int = NumberTypes.UOffsetTFlags.cast(offset(14))
	if o != 0:
		return vector_len(o)
	return 0


func color() -> int:
	var o: int = NumberTypes.UOffsetTFlags.cast(offset(16))
	if o != 0:
		return getVal(NumberTypes.Int8Flags, self._pos + o)
	return 2


func weapons(j: int, obj: Weapon = Weapon.new()) -> Weapon:
	var o: int = NumberTypes.UOffsetTFlags.cast(offset(18))
	if o != 0:
		obj._assign(_buffer, indirect(vector(o) + (j * 4)))
		return obj
	return null


func weapons_length() -> int:
	var o: int = NumberTypes.UOffsetTFlags.cast(offset(18))
	if o != 0:
		return vector_len(o)
	return 0


func equipped_type() -> int:
	var o: int = NumberTypes.UOffsetTFlags.cast(offset(20))
	if o != 0:
		return getVal(NumberTypes.Uint8Flags, self._pos + o)
	return 0


func equipped(obj: Weapon = Weapon.new()) -> Weapon:
	var o: int = NumberTypes.UOffsetTFlags.cast(offset(22))
	if o != 0:
		return union(obj, self._pos + o) as Weapon
	return null


static func start_monster(builder: FlatBuffersBuilder) -> void:
	builder.start_table(10)

static func add_pos(builder: FlatBuffersBuilder, pos_offset: int) -> void:
	builder.prepend_struct_slot(0, pos_offset, 0)

static func add_mana(builder: FlatBuffersBuilder, mana: int) -> void:
	builder.prepend_uint16_slot(1, mana, 150)

static func add_hp(builder: FlatBuffersBuilder, hp: int) -> void:
	builder.prepend_uint16_slot(2, hp, 100)

static func add_name(builder: FlatBuffersBuilder, name_offset: int) -> void:
	builder.prepend_UOffsetTRelativeSlot(3, name_offset, 0)

static func add_inventory(builder: FlatBuffersBuilder, inventory_offset: int) -> void:
	builder.prepend_UOffsetTRelativeSlot(5, inventory_offset, 0)

static func create_inventory_vector(builder: FlatBuffersBuilder, inventory: Array) -> int:
	return builder.create_byte_vector(inventory)

static func start_inventory_vector(builder: FlatBuffersBuilder, num_elems: int) -> void:
	builder.start_vector(1, num_elems, 1)

static func add_color(builder: FlatBuffersBuilder, color: int) -> void:
	builder.prepend_byte_slot(6, color, 2)

static func add_weapons(builder: FlatBuffersBuilder, weapons_offset: int) -> void:
	builder.prepend_UOffsetTRelativeSlot(7, weapons_offset, 0)

static func create_weapons_vector(builder: FlatBuffersBuilder, weapons_offsets: Array) -> int:
	builder.start_vector(4, weapons_offsets.size(), 4)
	for i in range(weapons_offsets.size() - 1, -1, -1):
		builder.prepend_UOffsetTRelative(weapons_offsets[i])

	return builder.end_vector()

static func start_weapons_vector(builder: FlatBuffersBuilder, num_elems: int) -> void:
	builder.start_vector(4, num_elems, 4)

static func add_equipped_type(builder: FlatBuffersBuilder, equipped_type: int) -> void:
	builder.prepend_byte_slot(8, equipped_type, 0)

static func add_equipped(builder: FlatBuffersBuilder, equipped_offset: int) -> void:
	builder.prepend_UOffsetTRelativeSlot(9, equipped_offset, 0)

static func end_monster(builder: FlatBuffersBuilder) -> int:
	return builder.end_table()

static func finish_monster_buffer(builder: FlatBuffersBuilder, offset: int) -> void:
	builder.finish(offset)
