# All structs in the generated code derive from this class, and add their own accessors.
class_name Struct
extends Reference

const Utils       = preload("Utils.gd")
const NumberTypes = preload("NumberTypes.gd")

# The underlying StreamPeerBuffer to hold the data of the Table.
var _buffer: StreamPeerBuffer
# Used to hold the position of the `_buffer` buffer.
var _pos: int


func _reset(buffer: StreamPeerBuffer, pos: int):
	NumberTypes.enforce_value(NumberTypes.UOffsetTFlags, pos)
	
	self._buffer = buffer
	if self._buffer != null:
		self._pos = pos
	else:
		self._pos = 0


func get_val(flags, off):
	"""
	Get retrieves a value of the type specified by `flags`  at the
	given offset.
	"""
	NumberTypes.enforce_value(NumberTypes.UOffsetTFlags, off)
	return Utils.get_val(flags, self._buffer, off)
