extends Control

#const FlatBuffersBuilder = preload("FlatBuffersGDScript/FlatBuffersBuilder.gd")

# Called when the node enters the scene tree for the first time.
func _ready():
	var builder: FlatBuffersBuilder = FlatBuffersBuilder.new(1)
	
	var weapon_one_name: int = builder.create_string("Spada1")
	var weapon_one_damage: int = 20
	var weapon_two_name: int = builder.create_string("Ascia2")
	var weapon_two_damage: int = 40
	
	var weaps: Array = [0, 0]
	weaps[0] = Weapon.create_weapon(builder, weapon_one_name, weapon_one_damage)
	weaps[1] = Weapon.create_weapon(builder, weapon_two_name, weapon_two_damage)
	
	var monster_name: int = builder.create_string("LalloFranco")
	var inventory: Array = [123, 31, 255, 0, 12, 57]
	var inv_offset: int = Monster.create_inventory_vector(builder, inventory)
	var weaps_offset: int = Monster.create_weapons_vector(builder, weaps)
	var pos_offset: int = Vec3.create_vec3(builder, 32.0, 12.0, 43.0)
	
	Monster.start_monster(builder)
	Monster.add_pos(builder, pos_offset)
	Monster.add_name(builder, monster_name)
	Monster.add_color(builder, Monster.MonsterColor.GREEN)
	Monster.add_hp(builder, 2500)
	Monster.add_inventory(builder, inv_offset)
	Monster.add_weapons(builder, weaps_offset)
	Monster.add_equipped_type(builder, Monster.Equipment.Weapon)
	Monster.add_equipped(builder, weaps[0])
	var monster_offset: int = Monster.end_monster(builder)
	Monster.finish_monster_buffer(builder, monster_offset)
	
	#_print_stream_buffer(builder.output())
	print_monster(builder.output())
	get_tree().quit(0)


func print_monster(buffer: StreamPeerBuffer):
	var monster: Monster = Monster.get_root_as_monster(buffer, Monster.new())
	print("Name: ", monster.name())
	print("Color: ", monster.color())
	print("HP: ", monster.hp())
	
	print("X: ", monster.pos().x())
	print("Y: ", monster.pos().y())
	print("Z: ", monster.pos().z())
	
	print("Inventory Length: ", monster.inventory_length())
	for i in range(0, monster.inventory_length()):
		print(("\tInventory %d: " % i), monster.inventory(i))
		
	print("Weapons Length: ", monster.weapons_length())
	print("Weapons 1 name: ", monster.weapons(1).name())
	print("Weapons 1 damage: ", monster.weapons(1).damage())
	
	for i in range(0, monster.weapons_length()):
		print(("\tWeapon %d name: " % i), monster.weapons(i).name())
		print(("\tWeapon %d damage: " % i), monster.weapons(i).damage())
		
	print("EquippedType: ", monster.equipped_type())
	print("Equipped name: ", monster.equipped().name())
	print("Equipped damage: ", monster.equipped().damage())


func _print_stream_buffer(stream_buffer: StreamPeerBuffer):
	var available_bytes: int = stream_buffer.get_available_bytes()
	print("Size: %d, available: %d" % [stream_buffer.get_size(), available_bytes])
	for i in range(0, available_bytes):
		print(stream_buffer.get_8())
	stream_buffer.seek(stream_buffer.get_size() - available_bytes)
