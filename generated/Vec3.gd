class_name Vec3 extends Struct


func _assign(buffer: StreamPeerBuffer, offset: int):
	_reset(buffer, offset)


func x() -> float:
	return getVal(NumberTypes.FloatFlags, _pos + 0)


func y() -> float:
	return getVal(NumberTypes.FloatFlags, _pos + 4)


func z() -> float:
	return getVal(NumberTypes.FloatFlags, _pos + 8)


static func create_vec3(builder: FlatBuffersBuilder, x: float, y: float, z: float) -> int:
	builder.prep(4, 12)
	builder.prepend_float(z)
	builder.prepend_float(y)
	builder.prepend_float(x) 
	return builder.offset()
