class_name Weapon extends Table


static func get_root_as_weapon(buffer: StreamPeerBuffer, 
								obj: Weapon,
								offset: int = -1) -> Weapon:
	buffer.set_big_endian(false)
	
	if offset == -1:
		offset = buffer.get_position()
	
	buffer.seek(offset)
	var pos: int = buffer.get_u32()
	
	obj._assign(buffer, pos + offset)
	return obj


func _assign(buffer: StreamPeerBuffer, offset: int):
	_reset(buffer, offset)


func name() -> String:
	var o: int = NumberTypes.UOffsetTFlags.cast(offset(4))
	if o != 0:
		return string(self._pos + o)
	return ""


func damage() -> int:
	var o: int = NumberTypes.UOffsetTFlags.cast(offset(6))
	if o != 0:
		return getVal(NumberTypes.Int16Flags, self._pos + o)
	return 0


static func create_weapon(builder: FlatBuffersBuilder, name_offset: int, damage: int) -> int:
	start_weapon(builder)
	add_name(builder, name_offset)
	add_damage(builder, damage)
	return end_weapon(builder)


static func start_weapon(builder: FlatBuffersBuilder):
	builder.start_table(2)

static func add_name(builder: FlatBuffersBuilder, name_offset: int):
	builder.prepend_UOffsetTRelativeSlot(0, name_offset, 0)

static func add_damage(builder: FlatBuffersBuilder, damage: int):
	builder.prepend_int16_slot(1, damage, 0)

static func end_weapon(builder: FlatBuffersBuilder) -> int:
	return builder.end_table()
