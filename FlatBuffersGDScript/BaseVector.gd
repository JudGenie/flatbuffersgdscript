class_name BaseVector extends Reference

const Utils       = preload("Utils.gd")
const NumberTypes = preload("NumberTypes.gd")

# The underlying ByteBuffer to hold the data of the vector.
var _buffer: StreamPeerBuffer
# Used to hold the vector data position. 
var _vector: int
# Used to hold the vector size.
var _length: int
# Used to hold the vector element size in table.
var _element_size: int


func _vector() -> int:
	"""Get the start data of a vector.
	
	Returns the start of the vector data.
	"""
	return _vector


func _element(j: int) -> int:
	"""Gets the element position in vector's ByteBuffer.
	
	j An `int` index of element into a vector.
	Returns the position of the vector element in a ByteBuffer.
	"""
	return _vector + j * _element_size


func length() -> int:
	return _length


func _reset(vector: int = 0, element_size: int = 0, buffer: StreamPeerBuffer = null) -> void:
	_buffer = buffer
	if (_buffer != null):
		_vector = vector
		_length = Utils.getVal(NumberTypes.Uint32Flags, 
								_buffer, 
								_vector - NumberTypes.Constants.SIZEOF_INT)
		_element_size = element_size
	else:
		_vector = 0
		_length = 0
		_element_size = 0
